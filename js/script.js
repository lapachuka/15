var app = {
    Model: {},
    Collection: {},
    Views: {},
    ViewEl: {}
};


app.Model = Backbone.Model.extend({});

app.Collection = Backbone.Collection.extend({
    model: app.Model
});


app.ViewEl = Backbone.View.extend({
    template: _.template($("#element-template").html()),
    events: {
        "click .element": "move"
    },
    render: function(){
        var dict = this.model.toJSON();
        var html = this.template(dict);
        this.$el.html(html);
        
        if ( this.model.get('value') == ""){
            this.$el.attr( "class","empty" );
        }
        return this;
    },
    move: function( e ) {      
        var moveTo = this.checkEmptyEl(this.model);
        if (moveTo != false){
            this.moveElements(moveTo);
        }
        elementsView.render();
        if(this.checkCorrectOrder() == true){
            alert('success!');
        }
    },
    checkCorrectOrder: function(){
        var order_array = [];
                
        this.collection.each(function(el) {
            //order_array.append(el.get('value'));
            if( el.get('value') == ''){
                order_array.push('9');
            }else{
                order_array.push(el.get('value'));
            }
        }, this);
        for( var i=0; i< order_array.length; i++){
            if( order_array[i] > order_array[i+1]){
                return false;
            }
        }
        return true;
    },
    moveElements: function( moveTo ){
        var key = Object.keys(moveTo)[0]; 
        if ( key == 'row' ){
            if (moveTo[key] > 0){
                for (var i=0; i<moveTo[key]; i++){
                    this.moveEmptyToOnePosition('left');
                }
            }else if(moveTo[key] < 0){
                for (var i=0; i< Math.abs(moveTo[key]); i++){
                    this.moveEmptyToOnePosition('right');
                }
            }else{
                return false
            }
        }else if( key == 'column'){
            if (moveTo[key] > 0){
                for (var i=0; i<moveTo[key]; i++){
                    this.moveEmptyToOnePosition('top');
                }
            }else if(moveTo[key] < 0){
                for (var i=0; i< Math.abs(moveTo[key]); i++){
                    this.moveEmptyToOnePosition('down');
                }
            }else{
                return false
            }
        }else{
            return false
        }
    },
    moveEmptyToOnePosition: function( way ){
        var emptyEl = this.collection.where({value:""})[0];
        var that = this;
        switch(way) {
            case 'left':
                var el = that.collection.where({column: (emptyEl.get('column') - 1).toString(), row: emptyEl.get('row') })[0];
                var extraEl = emptyEl.get('value'); 
                emptyEl.set({'value': el.get('value')});
                el.set({'value': extraEl });               
                break;
            case 'right':
                var col =  parseInt(emptyEl.get('column')) +1;
                var el = that.collection.where({column: col.toString(), row: emptyEl.get('row') })[0];
                var extraEl = emptyEl.get('value'); 
                emptyEl.set({'value': el.get('value')});
                el.set({'value': extraEl });               
                break;
             case 'top':
                var row =  parseInt(emptyEl.get('row'))-1;
                var el = that.collection.where({column: emptyEl.get('column'), row: row.toString() })[0];
                var extraEl = emptyEl.get('value'); 
                emptyEl.set({'value': el.get('value')});
                el.set({'value': extraEl });               
                break;
              case 'down':
                var row =  parseInt(emptyEl.get('row'))+1;
                var el = that.collection.where({column: emptyEl.get('column'), row: row.toString() })[0];
                var extraEl = emptyEl.get('value'); 
                emptyEl.set({'value': el.get('value')});
                el.set({'value': extraEl });               
                break;
            default:
                return false;
        }
    },
    checkEmptyEl: function( curEl ){
        var emptyEl = this.collection.where({value:""})[0];       
        if( emptyEl.get('row') == curEl.get('row') ){    
            var result = emptyEl.get('column') - curEl.get('column');
            return { row: result };
        }else if( emptyEl.get('column') == curEl.get('column') ){
            var result = emptyEl.get('row') - curEl.get('row');
            return { column: result };
        }else{
            return false;
        } 
    },
    randomizeElements: function(){
        var that = this;
        for (var i = 0; i < 100; i ++){
            var number = Math.floor((Math.random(9) * 8) +1);
            var el = that.collection.where({value: number.toString()})[0];
            var moveTo = that.checkEmptyEl(el);
            if (moveTo != false){
                this.moveElements(moveTo);
            }
        }
        elementsView.render();
    }
});

app.Views = Backbone.View.extend({
    tagName: 'div',
    className: 'container-15',
    render: function() {
        this.$el.html('');
        var that = this;
        this.collection.each(function(el) {
            var elView = new app.ViewEl({model: el, collection: this.collection});
            this.$el.append( elView.render().el);
        }, this);
        
        return this;
    },
    randomizeElements: function(){
        var some = new app.ViewEl({collection: this.collection}).randomizeElements();
    }

});

var collections = new app.Collection([
    {row: '1', column: '1', value: '1'},
    {row: '1', column: '2', value: '2'},
    {row: '1', column: '3', value: '3'},
    {row: '2', column: '1', value: '4'},
    {row: '2', column: '2', value: '5'},
    {row: '2', column: '3', value: '6'},
    {row: '3', column: '1', value: '7'},
    {row: '3', column: '2', value: '8'},
    {row: '3', column: '3', value: ''}
]);

var elementsView = new app.Views({ collection: collections });

$('.container').append(elementsView.render().el);
elementsView.randomizeElements();
